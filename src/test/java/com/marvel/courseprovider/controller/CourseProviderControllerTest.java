package com.marvel.courseprovider.controller;

import com.marvel.courseprovider.model.CourseProvider;
import com.marvel.courseprovider.model.LoginDetails;
import com.marvel.courseprovider.model.ResponseDto;
import com.marvel.courseprovider.repository.CourseProviderManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class CourseProviderControllerTest {

    @Mock
    private CourseProviderManager courseProviderManager;

    @InjectMocks
    CourseProviderController courseProviderController;

    @Test
    public void register(){
        CourseProvider courseProvider = new CourseProvider();
        Mockito.when(courseProviderManager.registerCourseProvider(courseProvider)).thenReturn(courseProvider);
        ResponseEntity<ResponseDto> responseEntity = courseProviderController.register("header",courseProvider);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void login(){
        LoginDetails loginDetails = new LoginDetails();
        Mockito.when(courseProviderManager.checkUser(loginDetails)).thenReturn(loginDetails);
        ResponseEntity<ResponseDto> responseEntity = courseProviderController.login("header",loginDetails);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }
}