package com.marvel.courseprovider.controller;

import com.google.common.collect.Lists;
import com.marvel.courseprovider.model.*;
import com.marvel.courseprovider.repository.CourseManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CourseControllerTest {

    @Mock
    private CourseManager courseManager;

    @InjectMocks
    CourseController courseController;

    @Test
    public void newCourse(){
        Course course = getCourse();
        Mockito.when(courseManager.registerCourse(course)).thenReturn(course);
        ResponseEntity<ResponseDto>  responseEntity = courseController.newCourse("header",course);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void enrolCourse(){
        CourseEnrolment courseEnrolment = new CourseEnrolment();
        Mockito.when(courseManager.enrolCourse(courseEnrolment)).thenReturn(courseEnrolment);
        ResponseEntity<ResponseDto>  responseEntity = courseController.enrolCourse("header",courseEnrolment);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void fetchCourses(){
        Mockito.when(courseManager.fetchCourses()).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.fetchCourses("header");
        Assert.assertEquals(courseViewList.size(), 0);
    }

    @Test
    public void searchCourses(){
        CourseSearchParameters searchParameters = new CourseSearchParameters();
        Mockito.when(courseManager.searchCourses(searchParameters)).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.searchCourses("header",searchParameters);
        Assert.assertEquals(courseViewList.size(), 0);
    }

    @Test
    public void fetchMyCourses(){
        CourseSearchParameters searchParameters = new CourseSearchParameters();
        Mockito.when(courseManager.fetchMyCourses("studenID")).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.fetchMyCourses("header","studenID");
        Assert.assertEquals(courseViewList.size(), 0);
    }

    @Test
    public void fetchCoursesByStudentInterests(){
        StudentInterestSearchParameters searchParameters = new StudentInterestSearchParameters();
        Mockito.when(courseManager.fetchCoursesByStudentInterests(searchParameters)).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.fetchCoursesByStudentInterests("header",searchParameters);
        Assert.assertEquals(courseViewList.size(), 0);
    }

    @Test
    public void fetchCoursesByStudentSearchHistory(){
        CourseSearchParameters searchParameters = new CourseSearchParameters();
        Mockito.when(courseManager.getCourseSearchHistoryBy("studenID")).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.fetchCoursesByStudentSearchHistory("header","studenID");
        Assert.assertEquals(courseViewList.size(), 0);
    }


    @Test
    public void fetchCoursesByStudentLocation(){
        StudentInterestSearchParameters searchParameters = new StudentInterestSearchParameters();
        Mockito.when(courseManager.fetchCoursesByStudentLocation(searchParameters)).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.fetchCoursesByStudentLocation("header",searchParameters);
        Assert.assertEquals(courseViewList.size(), 0);
    }

    @Test
    public void fetchCourseProviderCourses(){
        CourseSearchParameters searchParameters = new CourseSearchParameters();
        Mockito.when(courseManager.fetchCourseProviderCourses("courseProviderId")).thenReturn(Lists.newArrayList());
        List<CourseView>  courseViewList = courseController.fetchCourseProviderCourses("header","courseProviderId");
        Assert.assertEquals(courseViewList.size(), 0);
    }

    @Test
    public void fetchCourseStudents(){
        CourseSearchParameters searchParameters = new CourseSearchParameters();
        Mockito.when(courseManager.fetchCourseStudents("courseProviderId")).thenReturn(Lists.newArrayList());
        List<CourseStudentsView>  courseViewList = courseController.fetchCourseStudents("header","courseProviderId");
        Assert.assertEquals(courseViewList.size(), 0);
    }



    private Course getCourse(){
        Course course = new Course();
        return course;
    }

}