package com.marvel.courseprovider.service;

import com.marvel.courseprovider.config.AWSConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.DecryptResponse;

@RunWith(MockitoJUnitRunner.class)
public class KMSServiceTest {

    @InjectMocks
    KMSService kmsService;

    @Mock
    private KmsClient kmsClient;

    @Mock
    private AWSConfiguration awsConfiguration;

    @Test
    public void decrypt() {
        String test = "test";
        SdkBytes encryptedDataBytes = SdkBytes.fromByteArray(test.getBytes());
        DecryptRequest decryptRequest = DecryptRequest.builder()
                .ciphertextBlob(encryptedDataBytes)
                .keyId("kmdID")
                .build();
        DecryptResponse decryptResponse = DecryptResponse.builder().plaintext(encryptedDataBytes).build();
        Mockito.when(kmsClient.decrypt(Mockito.any(DecryptRequest.class))).thenReturn(decryptResponse);
        kmsService.decrypt(test.getBytes());
        Mockito.verify(kmsClient).decrypt(Mockito.any(DecryptRequest.class));
        Mockito.verify(awsConfiguration).getKmskeyid();
    }

}