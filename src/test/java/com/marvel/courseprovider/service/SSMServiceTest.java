package com.marvel.courseprovider.service;

import com.marvel.courseprovider.config.AWSConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.Assert;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;

@RunWith(MockitoJUnitRunner.class)
public class SSMServiceTest {

    @Mock
    SsmClient ssmClient;


    @Mock
    AWSConfiguration awsConfiguration;

    @InjectMocks
    SSMService ssmService;

    @Test
    public void getDBPassword() {
        String ssmDBKey = "ssmDBKey";
        GetParameterRequest parameterRequest = GetParameterRequest.builder()
                .name(ssmDBKey)
                .build();
        GetParameterResponse getParameterResponse = GetParameterResponse.builder().build();
        Mockito.when(awsConfiguration.getSsmDBKey()).thenReturn(ssmDBKey);
        Mockito.when(ssmClient.getParameter(Mockito.any(GetParameterRequest.class))).thenReturn(getParameterResponse);
        Assert.isNull(ssmService.getDBPassword());
        Mockito.verify(ssmClient).getParameter(Mockito.any(GetParameterRequest.class));
        Mockito.verify(awsConfiguration).getSsmDBKey();
    }

}