package com.marvel.courseprovider.service;

import com.marvel.courseprovider.config.AWSConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.nio.charset.StandardCharsets;

@RunWith(MockitoJUnitRunner.class)
public class PasswordServiceTest {
    @Mock
    KMSService kmsService;

    @Mock
    S3Service s3Service;

    @Mock
    AWSConfiguration awsConfiguration;

    @InjectMocks
    PasswordService passwordService;

    @Test
    public void getMarvelDBPassword(){
        String test = "test";
        String bucketName = "bucketName";
        String s3DBKey = "s3DBKey";
        byte[] bytesData = test.getBytes(StandardCharsets.UTF_8);
        Mockito.when(s3Service.fetchBytesFromBuckets(bucketName,s3DBKey)).thenReturn(bytesData);
        Mockito.when(kmsService.decrypt(bytesData)).thenReturn(test);
        Mockito.when(awsConfiguration.getS3BucketName()).thenReturn(bucketName);
        Mockito.when(awsConfiguration.getS3DBKey()).thenReturn(s3DBKey);
        Mockito.when(kmsService.decrypt(bytesData)).thenReturn(test);
        String password = passwordService.getMarvelDBPassword();
        Mockito.verify(s3Service).fetchBytesFromBuckets(bucketName,s3DBKey);
        Mockito.verify(kmsService).decrypt(bytesData);
        Mockito.verify(awsConfiguration).getS3BucketName();
        Mockito.verify(awsConfiguration).getS3DBKey();
        Assert.assertEquals(password,test);
    }
}