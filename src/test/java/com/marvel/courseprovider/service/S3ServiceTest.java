package com.marvel.courseprovider.service;

import com.marvel.courseprovider.config.AWSConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;

@RunWith(MockitoJUnitRunner.class)
public class S3ServiceTest {

    @InjectMocks
    S3Service s3Service;

    @Mock
    private S3Client s3Client;

    @Mock
    AWSConfiguration awsConfiguration;

    @Test
    public void fetchBytesFromBuckets(){
        GetObjectResponse getObjectResponse = GetObjectResponse.builder().build();
        ResponseBytes<GetObjectResponse> responseResponseBytes = ResponseBytes.fromByteArray(getObjectResponse,"".getBytes());
        Mockito.when(s3Client.getObjectAsBytes(Mockito.any(GetObjectRequest.class))).thenReturn(responseResponseBytes);
        s3Service.fetchBytesFromBuckets("bucketName","key");
        Mockito.verify(s3Client).getObjectAsBytes(Mockito.any(GetObjectRequest.class));
    }
}