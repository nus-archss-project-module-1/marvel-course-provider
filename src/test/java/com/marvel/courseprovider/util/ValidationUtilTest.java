package com.marvel.courseprovider.util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ValidationUtilTest {


    @Test
    public void numericValidation() {
        Assert.assertTrue(ValidationUtil.isAlphaNumeric("1220"));
    }

    @Test
    public void alphabetValidation() {
        Assert.assertTrue(ValidationUtil.isAlphaNumeric("test"));
    }

    @Test
    public void specialCaharcterValidation() {
        Assert.assertFalse(ValidationUtil.isAlphaNumeric("test%&^%&^%&%"));
    }

}