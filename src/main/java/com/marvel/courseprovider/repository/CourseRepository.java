package com.marvel.courseprovider.repository;

import com.marvel.courseprovider.model.Course;
import com.marvel.courseprovider.model.CourseView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    @Query("select new com.marvel.courseprovider.model.CourseView(c.id, c.courseProviderId, c.name, c.category, c.subCategory, c.classSize, " +
            "c.ageGroup, c.grades, c.location, c.teacher, c.modules, c.duration,c.fee, c.level, c.frequency, c.slots, cp.name) " +
            "from Course c, CourseProvider cp where c.courseProviderId = cp.id ")
    List<CourseView> fetchCourses();


    @Query(value = "select " +
            "new com.marvel.courseprovider.model.CourseView(c.id, c.courseProviderId, c.name, c.category, c.subCategory, c.classSize," +
            "  c.ageGroup, c.grades, c.location, c.teacher, c.modules, c.duration,c.fee, c.level, c.frequency, c.slots, c.name) " +
            " from Course c " +
            "where (:category='' or c.category=:category) and " +
            "(:subCategory='' or c.subCategory=:subCategory) and " +
            "(:location='' or c.location=:location) and " +
            "(:ageGroup='' or c.ageGroup=:ageGroup)")
    List<CourseView> searchCourses(@Param("category") String category,
                                   @Param("subCategory") String subCategory,
                                   @Param("location") String location,
                                   @Param("ageGroup") String ageGroup);

    @Query(value = "select " +
            "new com.marvel.courseprovider.model.CourseView(c.id, c.courseProviderId, c.name, c.category, c.subCategory, c.classSize," +
            "  c.ageGroup, c.grades, c.location, c.teacher, c.modules, c.duration,c.fee, c.level, c.frequency, c.slots, c.name) " +
            " from Course c " +
            "where (c.subCategory in :studentInterests)")
    List<CourseView> fetchCoursesByStudentInterests(@Param("studentInterests") List<String> studentInterests);

    @Query(value = "select " +
            "new com.marvel.courseprovider.model.CourseView(c.id, c.courseProviderId, c.name, c.category, c.subCategory, c.classSize," +
            "  c.ageGroup, c.grades, c.location, c.teacher, c.modules, c.duration,c.fee, c.level, c.frequency, c.slots, c.name) " +
            " from Course c " +
            "where c.location = :studentLocation")
    List<CourseView> fetchCoursesByStudentLocation(@Param("studentLocation") String studentLocation);
}