package com.marvel.courseprovider.repository;

import com.marvel.courseprovider.model.CourseProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseProviderRepository extends CrudRepository<CourseProvider, Long> {

}