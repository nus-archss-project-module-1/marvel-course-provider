package com.marvel.courseprovider.repository;

import com.marvel.courseprovider.model.CourseProvider;
import com.marvel.courseprovider.model.LoginDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 9:29 PM
 */

@Component
public class CourseProviderManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseProviderManager.class);

    @Autowired
    private CourseProviderRepository courseProviderRepository;
    @Autowired
    private LoginDetailsRepository loginDetailsRepository;

    @Transactional
    public CourseProvider registerCourseProvider(CourseProvider courseProvider) {
        LOGGER.info("Registering course provider: [{}, {}]", courseProvider.getName(), courseProvider.getEmail());
        LoginDetails loginDetails = new LoginDetails(courseProvider.getEmail(), courseProvider.getUserPassword(), courseProvider.getId(), courseProvider.getName(), courseProvider.getLoginType());
        loginDetailsRepository.save(loginDetails);
        CourseProvider save = courseProviderRepository.save(courseProvider);
        LOGGER.info("Successfully registered course provider: [{}, {}]", courseProvider.getName(), courseProvider.getEmail());
        return save;
    }

    public LoginDetails checkUser(LoginDetails loginDetails) {
        Optional<LoginDetails> dbUserObject = loginDetailsRepository.findById(loginDetails.getUserName());
        LOGGER.info("User object in database: {}", dbUserObject);
        if (dbUserObject.isPresent()) {
            LOGGER.info("dbUserObject.get().getLoginType(): {}", dbUserObject.get().getLoginType());
            LOGGER.info("loginDetails.getLoginType(): {}", loginDetails.getLoginType());
            if(dbUserObject.get().getLoginType().equals(loginDetails.getLoginType())) {
                LoginDetails details = dbUserObject.get();
                return details;
            }
//            LoginDetails details = dbUserObject.get();
//            return details;
        }
        return null;
    }
}
