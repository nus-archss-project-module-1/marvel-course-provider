package com.marvel.courseprovider.repository;

import com.marvel.courseprovider.model.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Author Vinit Kumar
 * @create 4/9/2021 9:29 PM
 */

@Component
public class CourseManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseManager.class);

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private CourseProviderRepository courseProviderRepository;
    @Autowired
    private CourseEnrolmentRepository courseEnrolmentRepository;
    @Autowired
    private CourseSearchHistoryRepository courseSearchHistoryRepository;

    @Transactional
    public Course registerCourse(Course course) {
        LOGGER.info("Registering course : [{}, {}]", course.getName(), course.getCategory());
        Course save = courseRepository.save(course);
        LOGGER.info("Successfully registered course : [{}, {}]", course.getName(), course.getCategory());
        return save;
    }

    public List<CourseView> fetchCourses() {
        LOGGER.info("Fetching all courses course");
        List<CourseView> courses = courseRepository.fetchCourses();
        LOGGER.info("Successfully fetch course Views : [{}]", courses);
        return courses;
    }

    @Transactional
    public List<CourseView> searchCourses(CourseSearchParameters searchParameters) {
        LOGGER.info("Searching for courses with : [{}] ", searchParameters);
        CourseSearchHistory courseSearchHistory = CourseSearchHistory.getCourseSearchHistory(searchParameters);
        courseSearchHistoryRepository.save(courseSearchHistory);
        List<CourseView> courses = courseRepository.searchCourses(searchParameters.getCategory(), searchParameters.getSubCategory(), searchParameters.getLocation(), searchParameters.getAgeGroup());
        LOGGER.info("Successfully searchCourses course Views : [{}]", courses);
        return courses;
    }

    public List<CourseView> fetchCoursesByStudentInterests(StudentInterestSearchParameters searchParameters) {
        LOGGER.info("Searching for courses with : [{}] ", searchParameters);
        List<String> studentInterests = Arrays.asList(StringUtils.split(searchParameters.getInterests(), ","));
        List<CourseView> courses = courseRepository.fetchCoursesByStudentInterests(studentInterests);
        LOGGER.info("Successfully fetchCoursesByStudentInterests courses : [{}]", courses.size());
        return courses;
    }

    public List<CourseView> fetchCoursesByStudentLocation(StudentInterestSearchParameters searchParameters) {
        LOGGER.info("Searching fetchCoursesByStudentLocation : [{}] ", searchParameters);
        List<CourseView> courses = courseRepository.fetchCoursesByStudentLocation(searchParameters.getLocation());
        LOGGER.info("Successfully fetchCoursesByStudentLocation courses : [{}]", courses.size());
        return courses;
    }

    public List<CourseView> fetchMyCourses(String studentId) {
        LOGGER.info("fetchMyCourse for student id: [{}]", studentId);
        List<CourseView> courses = courseEnrolmentRepository.fetchMyCourses(studentId);
        LOGGER.info("Successfully fetchMyCourse : [{}]", courses);
        return courses;
    }

    public List<CourseView> fetchCourseProviderCourses(String courseProviderId) {
        LOGGER.info("fetchMyCourse for student id: [{}]", courseProviderId);
        List<CourseView> courses = courseEnrolmentRepository.fetchCourseProviderCourses(courseProviderId);
        LOGGER.info("Successfully fetchCourseProviderCourses : [{}]", courses);
        return courses;
    }

    public List<CourseStudentsView> fetchCourseStudents(String courseProviderId) {
        LOGGER.info("fetchCourseStudents for student id: [{}]", courseProviderId);
        List<CourseStudentsView> courses = courseEnrolmentRepository.fetchCourseStudents(courseProviderId);
        LOGGER.info("Successfully fetchCourseStudents : [{}]", courses);
        return courses;
    }

    public CourseEnrolment enrolCourse(CourseEnrolment courseEnrolment) {
        LOGGER.info("Enrolling course : [{}]", courseEnrolment);
        CourseEnrolment save = courseEnrolmentRepository.save(courseEnrolment);
        LOGGER.info("Successfully registered course : [{}]", courseEnrolment);
        return save;
    }

    public List<CourseView> getCourseSearchHistoryBy(String studentId) {
        LOGGER.info("getCourseSearchHistoryBy for student id: [{}]", studentId);
        List<CourseSearchHistory> courses = courseSearchHistoryRepository.getCourseSearchHistoryBy(studentId);
        List<String> subCategories = courses.stream().map(CourseSearchHistory::getSubCategory)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        List<CourseView> courseViews = courseRepository.fetchCoursesByStudentInterests(subCategories);
        LOGGER.info("Successfully getCourseSearchHistoryBy : [{}]", courses);
        return courseViews;
    }
}
