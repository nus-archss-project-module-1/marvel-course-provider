package com.marvel.courseprovider.repository;

import com.marvel.courseprovider.model.CourseEnrolment;
import com.marvel.courseprovider.model.CourseStudentsView;
import com.marvel.courseprovider.model.CourseView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseEnrolmentRepository extends JpaRepository<CourseEnrolment, String> {

    @Query("select new com.marvel.courseprovider.model.CourseView(c.id, c.courseProviderId, c.name, c.category, c.subCategory, c.classSize, " +
            "c.ageGroup, c.grades, c.location, c.teacher, c.modules, c.duration,c.fee,c.slots,c.level,c.frequency, cp.name) " +
            "from CourseEnrolment ce join Course c on ce.courseId = c.id\n" +
            "    join CourseProvider cp on c.courseProviderId = cp.id\n" +
            "    where ce.studentId in (?1)")
    List<CourseView> fetchMyCourses(String studentId);

    @Query("select new com.marvel.courseprovider.model.CourseView(c.id, c.courseProviderId, c.name, c.category, c.subCategory, c.classSize, " +
            "c.ageGroup, c.grades, c.location, c.teacher, c.modules, c.duration,c.fee,c.slots,c.level,c.frequency, cp.name) " +
            "from Course c join CourseProvider cp on c.courseProviderId = cp.id\n" +
            "    where c.courseProviderId in (?1)")
    List<CourseView> fetchCourseProviderCourses(String courseProviderId);

    @Query("select new com.marvel.courseprovider.model.CourseStudentsView(ce.id, c.name, c.id, ce.studentId, ce.studentName, ce.gender, ce.parentName, " +
            "ce.contactNumber, ce.email, c.teacher) " +
            "from CourseEnrolment  ce join Course c on ce.courseId=c.id " +
            "join CourseProvider cp on c.courseProviderId = cp.id\n" +
            "    where c.courseProviderId in (?1)")
    List<CourseStudentsView> fetchCourseStudents(String courseProviderId);
}