package com.marvel.courseprovider.repository;

import com.marvel.courseprovider.model.CourseSearchHistory;
import com.marvel.courseprovider.model.CourseView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseSearchHistoryRepository extends JpaRepository<CourseSearchHistory, String> {
    @Query("select new com.marvel.courseprovider.model.CourseSearchHistory(c.id, c.studentId, c.category, c.subCategory, c.ageGroup, c.location) " +
            "from CourseSearchHistory c where c.studentId in (?1)")
    List<CourseSearchHistory> getCourseSearchHistoryBy(String studentId);
}