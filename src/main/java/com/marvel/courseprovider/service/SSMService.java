package com.marvel.courseprovider.service;

import com.marvel.courseprovider.config.AWSConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;

import javax.annotation.PostConstruct;

@Service
public class SSMService {

    SsmClient ssmClient;


    @Autowired
    AWSConfiguration awsConfiguration;

    @PostConstruct
    public void initialize() {
        AwsBasicCredentials awscred = AwsBasicCredentials.create(awsConfiguration.getAccesskeyid(), awsConfiguration.getSeceretKeyAccess());
        AwsCredentialsProvider awsCredentialsProvider = StaticCredentialsProvider.create(awscred);
        ssmClient = SsmClient.builder().region(Region.AP_SOUTHEAST_1).credentialsProvider(awsCredentialsProvider).build();
    }


    public String getDBPassword() {
        GetParameterRequest parameterRequest = GetParameterRequest.builder()
                .name(awsConfiguration.getSsmDBKey())
                .build();

        GetParameterResponse parameterResponse = ssmClient.getParameter(parameterRequest);
        return null;
    }
}
