package com.marvel.courseprovider.service;

import com.marvel.courseprovider.config.AWSConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.DecryptResponse;

import javax.annotation.PostConstruct;

@Service
public class KMSService {

    private KmsClient kmsClient;

    @Autowired
    private AWSConfiguration awsConfiguration;

    @PostConstruct
    public void initialize(){
        AwsBasicCredentials awscred =   AwsBasicCredentials.create(awsConfiguration.getAccesskeyid(),awsConfiguration.getSeceretKeyAccess());
        AwsCredentialsProvider awsCredentialsProvider = StaticCredentialsProvider.create(awscred);
        kmsClient = KmsClient.builder()
                .region(Region.AP_SOUTHEAST_1).credentialsProvider(awsCredentialsProvider)
                .build();
    }

    public String decrypt(byte[] encryptedData){
        SdkBytes encryptedDataBytes = SdkBytes.fromByteArray(encryptedData);

        DecryptRequest decryptRequest = DecryptRequest.builder()
                .ciphertextBlob(encryptedDataBytes)
                .keyId(awsConfiguration.getKmskeyid())
                .build();

        DecryptResponse decryptResponse = kmsClient.decrypt(decryptRequest);
        SdkBytes plainText = decryptResponse.plaintext();
        return new String(plainText.asByteArray());
    }


}
