package com.marvel.courseprovider.config;

import com.marvel.courseprovider.service.PasswordService;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class CourseProviderConfiguration {

    private static final String JDBC_URL = "jdbc:mysql://marvel-db-encrypted.crql4ccx4jyt.ap-southeast-1.rds.amazonaws.com:3306/marvel_courses_db";

    @Bean
    public DataSource getDataSource(PasswordService passwordService,AWSConfiguration awsConfiguration) {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.username("marvel_courses_rw");
        dataSourceBuilder.password(passwordService.getMarvelDBPassword());
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
        dataSourceBuilder.url(JDBC_URL);
         return dataSourceBuilder.build();
    }
}
