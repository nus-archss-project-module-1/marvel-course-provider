package com.marvel.courseprovider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/27/2021 12:30 AM
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "course_enrolment")
public class CourseEnrolment {

    @Id
    private String id;
    private String courseId;
    private String studentId;
    private String studentName;
    private String gender;
    private String parentName;
    private String contactNumber;
    private String email;

    public CourseEnrolment() {
        this.id = String.valueOf(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
