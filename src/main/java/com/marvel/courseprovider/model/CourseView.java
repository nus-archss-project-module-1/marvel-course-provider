package com.marvel.courseprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Pradeep Kumar
 * @create 4/20/2021 1:15 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseView {

    protected String id;
    protected String courseProviderId;
    protected String name;
    protected String category;
    protected String subCategory;
    protected Integer classSize;
    protected String ageGroup;
    protected String grades;
    protected String location;
    protected String teacher;
    protected String modules;
    protected Integer duration;
    protected Integer fee;
    private String level;
    private String frequency;
    private String slots;
    private String courseProviderName;
}
