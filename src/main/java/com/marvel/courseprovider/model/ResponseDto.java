package com.marvel.courseprovider.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 11:53 PM
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDto {

    private final String id;
    private final String name;
    private String message;

    public ResponseDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public ResponseDto(String id, String name, String message) {
        this.id = id;
        this.name = name;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
