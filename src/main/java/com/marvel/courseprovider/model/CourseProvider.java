package com.marvel.courseprovider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.*;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 4:55 PM
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "course_provider")
public class CourseProvider {
    @Id
    @Column(name = "id")
    private final String id;
    private String name;
    @Column(name = "license_number")
    private String licenseNumber;
    @Column(name = "license_document")
    private String licenseDocument;
    @Column(name = "contact_number")
    private String contactNumber;
    private String email;
    private String address;
    @Column(name = "location")
    private String location;
    private String country;
    @Column(name = "postal_code")
    private long postalCode;
    private String userPassword;
    private String confirmUserPassword;
    private String loginType;

    public CourseProvider() {
        this.id = String.valueOf(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
