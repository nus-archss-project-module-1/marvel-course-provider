package com.marvel.courseprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Pradeep Kumar
 * @create 4/20/2021 1:15 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseStudentsView {

    private String id;
    private String name;
    private String courseId;
    private String studentId;
    private String studentName;
    private String gender;
    private String parentName;
    private String contactNumber;
    private String email;
    private String teacher;
}
