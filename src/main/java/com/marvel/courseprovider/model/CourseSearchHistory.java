package com.marvel.courseprovider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/27/2021 12:30 AM
 */
@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "course_search_history")
public class CourseSearchHistory {

    @Id
    private String id;
    @Column(name = "student_id")
    private String studentId;
    private String category;
    @Column(name = "sub_category")
    private String subCategory;
    @Column(name = "age_group")
    private String ageGroup;
    private String location;

    public CourseSearchHistory() {
        this.id = String.valueOf(System.currentTimeMillis());
    }

    public static CourseSearchHistory getCourseSearchHistory(CourseSearchParameters searchParameters) {
        CourseSearchHistory searchHistory = new CourseSearchHistory();
        searchHistory.setStudentId(searchParameters.getStudentId());
        searchHistory.setCategory(searchParameters.getCategory());
        searchHistory.setSubCategory(searchParameters.getSubCategory());
        searchHistory.setAgeGroup(searchParameters.getAgeGroup());
        searchHistory.setLocation(searchParameters.getLocation());
        return searchHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CourseSearchHistory that = (CourseSearchHistory) o;

        return new EqualsBuilder().append(id, that.id).append(studentId, that.studentId).append(category, that.category).append(subCategory, that.subCategory).append(ageGroup, that.ageGroup).append(location, that.location).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(studentId).append(category).append(subCategory).append(ageGroup).append(location).toHashCode();
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
