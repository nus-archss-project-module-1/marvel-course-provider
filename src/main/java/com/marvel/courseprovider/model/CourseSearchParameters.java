package com.marvel.courseprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/29/2021 12:53 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseSearchParameters {
    private String studentId;
    private String category;
    private String subCategory;
    private String ageGroup;
    private String location;

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
