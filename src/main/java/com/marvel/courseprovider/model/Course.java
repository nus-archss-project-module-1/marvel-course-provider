package com.marvel.courseprovider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/11/2021 5:07 PM
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "courses")
public class Course {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "course_provider_id")
    private String courseProviderId;
    private String name;
    private String category;
    private String subCategory;
    private String level;
    private Integer classSize;
    private String ageGroup;
    private String grades;
    private String preRequisite;
    private String teacher;
    private String modules;
    private Integer duration;
    private Integer fee;
    private String slots;
    private String frequency;
    private String location;
    private String loginType;

    public Course() {
        this.id = String.valueOf(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
