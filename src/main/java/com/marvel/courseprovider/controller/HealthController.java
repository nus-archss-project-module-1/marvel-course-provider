package com.marvel.courseprovider.controller;

import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("marvel-course-provider")
public class HealthController {

    @GetMapping("/health")
    public String courseProviderServiceHealth(@RequestHeader("marvel-course-provider-request") String header) {
        return "Greetings from Marvel Course Provider!";
    }

    @GetMapping("/heartbit")
    public String heartBit() {
        return "Heartbit Course Provider!";
    }

    @ExceptionHandler({ MissingRequestHeaderException.class })
    public String handleException() {
        return "UnAuthorized Access to the Service";
    }
}
