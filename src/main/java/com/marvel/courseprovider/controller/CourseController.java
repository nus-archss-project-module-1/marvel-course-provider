package com.marvel.courseprovider.controller;

import com.google.common.collect.Lists;
import com.marvel.courseprovider.model.*;
import com.marvel.courseprovider.repository.CourseManager;
import com.marvel.courseprovider.util.ValidationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @Author Vinit Kumar
 * @create 4/8/2021 12:23 AM
 */
@RestController
@RequestMapping("marvel-course-provider")
public class CourseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseController.class);

    @Autowired
    private CourseManager courseManager;

    @PostMapping(value = "/newCourse", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDto> newCourse(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody Course course) {
        try {
            LOGGER.info("Incoming Course Registration information: {}, Request Header: {}", course, header);
            Course saved = courseManager.registerCourse(course);
            LOGGER.info(String.format("Successfully registered Course Provider: %s", saved.getName()));
            ResponseDto responseDto = getClientMessage(saved, "Successfully saved");
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            ResponseDto responseDto = getClientMessage(course, "Error occurred in the server");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(responseDto);
        }
    }

    @PostMapping(value = "/enrolCourse", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDto> enrolCourse(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody CourseEnrolment courseEnrolment) {
        try {
            LOGGER.info("Incoming Course Registration information: {}, Request Header: {}", courseEnrolment, header);
            CourseEnrolment saved = courseManager.enrolCourse(courseEnrolment);
            LOGGER.info(String.format("Successfully registered Course Enrolment ID: %s", saved.getId()));
            return new ResponseEntity<>(new ResponseDto(courseEnrolment.getCourseId(), "success"), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseDto("1", "fail"));
        }
    }

    @GetMapping(value = "/fetchCourses", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> fetchCourses(@RequestHeader ("marvel-course-provider-request") String header) {
        try {
            LOGGER.info("Fetch Courses. Request Header: {}", header);
            List<CourseView> fetchCourses = courseManager.fetchCourses();
            LOGGER.info("Successfully fetch courses : [{}]", fetchCourses.size());
            return fetchCourses;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @PostMapping(value = "/searchCourses", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> searchCourses(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody CourseSearchParameters searchParameters) {
        try {
            LOGGER.info("Searching Courses with request parameters: [{}], Request Header: [{}]", searchParameters, header);
            List<CourseView> searchedCourses = courseManager.searchCourses(searchParameters);
            LOGGER.info("Successfully search courses : [{}]", searchedCourses.size());
            return searchedCourses;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @GetMapping(value = "/fetchMyCourses/{studentId}", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> fetchMyCourses(@RequestHeader ("marvel-course-provider-request") String header, @PathVariable String studentId) {
        try {
            if(ValidationUtil.isAlphaNumeric(studentId)) {
                LOGGER.info("Fetching my Courses. Student ID: [{}], Request Header: [{}]", studentId, header);
                List<CourseView> fetchCourses = courseManager.fetchMyCourses(studentId);
                LOGGER.info("Successfully fetched myCourse: [{}] for student [{}]", fetchCourses.size(),studentId);
                return fetchCourses;
            }else{
                return Lists.newArrayList();
            }
         } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @PostMapping(value = "/fetchCoursesByStudentInterests", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> fetchCoursesByStudentInterests(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody StudentInterestSearchParameters searchParameters) {
        try {
            LOGGER.info("Searching Courses with request parameters: [{}], Request Header: [{}]", searchParameters, header);
            List<CourseView> searchedCourses = courseManager.fetchCoursesByStudentInterests(searchParameters);
            LOGGER.info("Successfully fetchCoursesByStudentInterests : [{}]", searchedCourses.size());
            return searchedCourses;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @GetMapping(value = "/fetchCoursesByStudentSearchHistory/{studentId}", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> fetchCoursesByStudentSearchHistory(@RequestHeader ("marvel-course-provider-request") String header, @PathVariable String studentId) {
        try {
            if(ValidationUtil.isAlphaNumeric(studentId)) {
                LOGGER.info("fetchCoursesByStudentSearchHistory with request parameters: [{}], Request Header: [{}]", studentId, header);
                List<CourseView> searchedCourses = courseManager.getCourseSearchHistoryBy(studentId);
                LOGGER.info("Successfully fetchCoursesByStudentSearchHistory : [{}]", searchedCourses.size());
                return searchedCourses;
            }else{
                return Lists.newArrayList();
            }
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @PostMapping(value = "/fetchCoursesByStudentLocation", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> fetchCoursesByStudentLocation(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody StudentInterestSearchParameters searchParameters) {
        try {
            LOGGER.info("FetchCoursesByStudentLocation with request parameters: [{}], Request Header: [{}]", searchParameters, header);
            List<CourseView> searchedCourses = courseManager.fetchCoursesByStudentLocation(searchParameters);
            LOGGER.info("Successfully fetchCoursesByStudentLocation : [{}]", searchedCourses.size());
            return searchedCourses;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @GetMapping(value = "/fetchCourseProviderCourses/{courseProviderId}", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseView> fetchCourseProviderCourses(@RequestHeader ("marvel-course-provider-request") String header, @PathVariable String courseProviderId) {
        try {
            if(ValidationUtil.isAlphaNumeric(courseProviderId))
            {
                LOGGER.info("Fetching my Courses by Course Provider ID: [{}], Request Header: [{}]", courseProviderId, header);
                List<CourseView> fetchCourses = courseManager.fetchCourseProviderCourses(courseProviderId);
                LOGGER.info("Successfully fetchCourseProviderCourses : [{}]", fetchCourses.size());
                return fetchCourses;
            }else{
                return Lists.newArrayList();
            }
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @GetMapping(value = "/fetchCourseStudents/{courseProviderId}", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CourseStudentsView> fetchCourseStudents(@RequestHeader ("marvel-course-provider-request") String header, @PathVariable String courseProviderId) {
        try {
            if(ValidationUtil.isAlphaNumeric(courseProviderId)) {
                LOGGER.info("fetchCourseStudents by Course Provider ID: [{}], Request Header: [{}]", courseProviderId, header);
                List<CourseStudentsView> fetchCourses = courseManager.fetchCourseStudents(courseProviderId);
                LOGGER.info("Successfully fetchCourseStudents : [{}]", fetchCourses.size());
                return fetchCourses;
            }else{
                return Lists.newArrayList();
            }
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    private ResponseDto getClientMessage(Course object, String message) {
        String clientMessage = String.format("ID: [%s], Name: [%s], Status: [%s]", object.getId(), object.getName(), message);
        return new ResponseDto(object.getCourseProviderId(), object.getName(), clientMessage);
    }
}
