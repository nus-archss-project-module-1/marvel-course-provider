package com.marvel.courseprovider.controller;

import com.marvel.courseprovider.model.CourseProvider;
import com.marvel.courseprovider.model.LoginDetails;
import com.marvel.courseprovider.model.ResponseDto;
import com.marvel.courseprovider.repository.CourseProviderManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @Author Pradeep Kumar
 * @create 4/8/2021 12:23 AM
 */
@RestController
@RequestMapping("marvel-course-provider")
public class CourseProviderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseProviderController.class);

    @Autowired
    private CourseProviderManager courseProviderManager;

    @PostMapping(value = "/register", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDto> register(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody CourseProvider courseProvider) {
        try {
            LOGGER.info("Incoming Course Provider Registration information: {}, Request Header: [{}]", courseProvider, header);
            CourseProvider saved = courseProviderManager.registerCourseProvider(courseProvider);

            LOGGER.info(String.format("Successfully registered Course Provider: %s", saved.getName()));
            ResponseDto responseDto = getClientMessage(saved, "Successfully saved");
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            ResponseDto responseDto = getClientMessage(courseProvider, "Error occurred in the server");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(responseDto);
        }
    }

    @PostMapping(value = "/login", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDto> login(@RequestHeader ("marvel-course-provider-request") String header, @RequestBody LoginDetails loginDetails) {
        try {
            LOGGER.info("Incoming Course Provider Login: {}, Request Header: {}", loginDetails, header);

            LoginDetails user = courseProviderManager.checkUser(loginDetails);
            if (Objects.nonNull(user)) {
                LOGGER.info(String.format("User found in database: %s", loginDetails.getUserName()));
                ResponseDto responseDto = new ResponseDto(user.getId(), user.getName(), "Success");
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }

            LOGGER.info(String.format("User not found in database: %s", loginDetails.getUserName()));
            ResponseDto responseDto = new ResponseDto("error", "Failure", "Invalid username/password");
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Error occurred: {}", e.getMessage(), e);
            ResponseDto responseDto = new ResponseDto("error", "Name", "Exception occurred while trying to login");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(responseDto);
        }
    }

    private ResponseDto getClientMessage(CourseProvider object, String message) {
        String clientMessage = String.format("ID: [%s], Name: [%s], Status: [%s]", object.getId(), object.getEmail(), message);
        return new ResponseDto(object.getId(), object.getEmail(), clientMessage);
    }
}
