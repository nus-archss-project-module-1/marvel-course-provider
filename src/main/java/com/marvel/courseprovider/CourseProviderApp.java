package com.marvel.courseprovider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseProviderApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseProviderApp.class);

    public static void main(String[] args) {
        SpringApplication.run(CourseProviderApp.class, args);
        System.setProperty("jasypt.encryptor.password", "password");
        LOGGER.info("Marvel Course Provider Application is started successfully.");
    }
}
