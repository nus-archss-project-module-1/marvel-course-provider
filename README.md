# marvel-course-provider

## Docker Build
 docker build -t course-provider-latest .
 docker tag course-provider-latest veenu143/ marvel-course-provider:latest
 docker push veenu143/ marvel-course-provider:latest

## Start Kubernetes deployment
kubectl apply -f kube/mysql/
kubectl apply -f kube/course-provider/
kubectl apply -f kube/deploy/

## Delete Kubernetes Deployment
kubectl delete -f kube/deploy/
kubectl delete -f kube/course-provider/
kubectl delete -f kube/mysql/