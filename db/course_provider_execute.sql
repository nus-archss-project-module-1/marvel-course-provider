drop database marvel_courses_db;
create database marvel_courses_db;
use marvel_courses_db;

drop
user marvel_courses_rw;
CREATE
USER marvel_courses_rw IDENTIFIED BY 'marvel';

GRANT ALL PRIVILEGES ON marvel_courses_db.* TO
'marvel_courses_rw';

drop
user marvel_user_rw;
CREATE
USER marvel_user_rw IDENTIFIED BY 'marvel';

drop table marvel_courses_db.course_provider;
CREATE TABLE marvel_courses_db.course_provider
(
    id     			varchar(25)  NOT NULL ,
    name   			varchar(128) ,
    license_number   varchar(45)  ,
    license_document    varchar(128) ,
    contact_number    varchar(10)  ,
    email          varchar(50)  ,
    location       varchar(20) ,
    country        varchar(10)  ,
    address        varchar(100)  ,
    postal_code    int,
    user_password  varchar(10),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    -- ,UNIQUE KEY email_UNIQUE (email)
);

INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100000','Lotus Pvt.Ltd','7879728','','9111111','a','Serangoon','Singapore','123 yuw Road',822101,'a');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100001','Lily Pvt.Ltd','1279737','','9222222','lily@email.com','Punggol','Singapore','124 yuw Road',819828,'Li2@');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100002','Daisy Pvt.Ltd','6928264','','9333333','Daisy@email.com','Harbor Front','Singapore','125 yuw Road',811110,'Da3#');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100003','ABC Pvt.Ltd','9122739','','9444444','ABC@email.com','Sengkang','Singapore','126 yuw Road',818137,'AB4$');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100004','XYZ Pvt.Ltd','9817371','','9555555','XYZ@email.com','Punggol','Singapore','127 yuw Road',812390,'XY5%');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100005','Sparkle Pvt.Ltd','7678161','','9666666','Sparkle@email.com','Jurong East','Singapore','128 yuw Road',876543,'Sp6^');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100006','Unicorn Pvt.Ltd','8789191','','9777777','Unicorn@email.com','Serangoon','Singapore','129 yuw Road',812083,'Un7&');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100007','Austria Pvt.Ltd','8798319','','9888888','Austria@email.com','Harbor Front','Singapore','130 yuw Road',864103,'Au8*');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100008','Sunshine Pvt.Ltd','3451152','','9000000','Sunshine@email.com','Jurong East','Singapore','131 yuw Road',834502,'Su9(');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100009','Rainbow Pvt.Ltd','6273521','','9999999','rainbow@email.com','Toa Payoh','Singapore','132 yuw Road',800374,'Ra0)');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100010','Bright Pvt.Ltd','8651825','','9012345','Bright@email.com','Hougang','Singapore','133 yuw Road',877740,'Br7:');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100011','Academia Pvt.Ltd','4651421','','9876543','Academia@email.com','Toa Payoh','Singapore','134 yuw Road',811110,'Ac4>');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('1618823401648','sadfdsfds','','','','a','Tampines','','',0,'a');


drop table marvel_courses_db.login_details;
CREATE TABLE marvel_courses_db.login_details
(
    user_name  varchar(80) NOT NULL,
    user_password  varchar(10),
    id varchar(25) not null,
    name  varchar(80) not null,
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (user_name)
);

INSERT INTO marvel_courses_db.login_details (`user_name`,`user_password`,`id`,`name`) VALUES ('a','a','100000','Lotus Pvt.Ltd');

drop table marvel_courses_db.course_category;
create table marvel_courses_db.course_category(
	category_id int not null auto_increment,
    category_name varchar(20),
    primary key (category_id),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP
);

drop table marvel_courses_db.course_sub_category;
create table marvel_courses_db.course_sub_category(
	subcategory_id int not null auto_increment,
    category_id	int not null,
    subcategory_name varchar(50),
    primary key (subcategory_id),
    UNIQUE KEY unique_idx (subcategory_id, category_id)
);

drop table marvel_courses_db.course_search_history;
create table marvel_courses_db.course_search_history(
     id varchar(25)  NOT NULL,
     student_id varchar(25)  NOT NULL,
     category varchar(50),
     sub_category varchar(50),
     age_group varchar(10),
     location varchar(20),
     created_time timestamp DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1619975678175','1619943421292','Music','Guitar','','','2021-05-03 01:14:38');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1619975725481','1619943421292','Music','Ukelele','','','2021-05-03 01:15:25');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1619976478511','1619943421292','Language','Mandarin','','','2021-05-03 01:27:58');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1619976487428','1619943421292','Language','Tamil','','','2021-05-03 01:28:07');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1620040923630','1619943421292','Sports','BasketBall','','','2021-05-03 19:22:03');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1620040946111','1619943421292','Sports','Tennis','','','2021-05-03 19:22:26');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1620040981468','1619943421292','Language','Hindi','','','2021-05-03 19:23:01');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1620041148165','1619943421292','Art','Culinary Arts','','','2021-05-03 19:25:48');
INSERT INTO marvel_courses_db.course_search_history(`id`,`student_id`,`category`,`sub_category`,`age_group`,`location`,`created_time`) VALUES ('1620465938408','1619943421292','Sports','Badminton','','','2021-05-08 17:25:38');

drop table marvel_courses_db.course_enrolment;
create table marvel_courses_db.course_enrolment(
                                                   id varchar(25)  NOT NULL,
     course_id varchar(25)  NOT NULL,
     student_id varchar(25)  NOT NULL,
     student_name   varchar(80) ,
     gender         varchar(6) ,
     parent_name    varchar(128) ,
     contact_number varchar(10) ,
     email          varchar(80)  ,
     created_time timestamp DEFAULT CURRENT_TIMESTAMP,
     primary key (id),
     UNIQUE KEY unique_idx (student_id, course_id)
);


INSERT INTO marvel_courses_db.course_enrolment(`id`,`course_id`,`student_id`,`student_name`,`gender`,`parent_name`,`contact_number`,`email`,`created_time`) VALUES ('1619798970973','CR001','1618819554023','Robin','Male','Hood Brother','','r','2021-05-01 00:09:30');
INSERT INTO marvel_courses_db.course_enrolment(`id`,`course_id`,`student_id`,`student_name`,`gender`,`parent_name`,`contact_number`,`email`,`created_time`) VALUES ('1619961135620','CR015','1619943421292','Nicole','Male','Thomas','23423423','n','2021-05-02 21:12:15');
INSERT INTO marvel_courses_db.course_enrolment(`id`,`course_id`,`student_id`,`student_name`,`gender`,`parent_name`,`contact_number`,`email`,`created_time`) VALUES ('1619961340552','CR019','1619943421292','Nicole','Male','Thomas','23423423','n','2021-05-02 21:15:40');
INSERT INTO marvel_courses_db.course_enrolment(`id`,`course_id`,`student_id`,`student_name`,`gender`,`parent_name`,`contact_number`,`email`,`created_time`) VALUES ('1620041012262','CR010','1619943421292','Nicole','Male','Thomas','23423423','n','2021-05-03 19:23:32');

drop table marvel_courses_db.courses;
CREATE TABLE marvel_courses_db.courses
(
    id     			varchar(25)  NOT NULL,
    course_provider_id varchar(25)  NOT NULL, -- FK to Course_Provider
    name   			varchar(128) ,
    category        varchar(128)  ,
    sub_category    varchar(128) ,
    level varchar(20),
    class_size      int  ,
    age_group       varchar(50)  ,
    grades          varchar(10) ,
    pre_requisite   varchar(128),
    teacher         varchar(128)  ,
    modules         varchar(128)  ,
    duration        int  ,
    fee             int  ,
    slots varchar(10),
    frequency varchar(10),
    location varchar(20),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    -- ,UNIQUE KEY email_UNIQUE (email)
);

INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR001','100000','Lotus Pvt.Ltd','Language','Mandarin','Basic',10,'5 to 8','P1 to P3','','Mr.Tan','',45,130,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR002','100000','Lotus Pvt.Ltd','Language','French','Basic',15,'5 to 8','P1 to P3','','Ms.Chung','',45,130,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR003','100000','Lotus Pvt.Ltd','Language','German','Basic',15,'5 to 8','P1 to P3','','Ms.Patel','',45,130,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR004','100000','Lotus Pvt.Ltd','Language','Hindi','Basic',15,'5 to 8','P1 to P3','','Ms.Sarita','',45,100,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR005','100001','Lily Pvt.Ltd','Art','Speech and Drama','Basic',5,'5 to 8','P1 to P3','','Ms.Ashie','',45,120,'4.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR006','100001','Lily Pvt.Ltd','Art','Culinary Arts','Basic',10,'5 to 8','P1 to P3','','Ms.Ancy','',30,130,'4.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR007','100001','Lily Pvt.Ltd','Art','Visual Arts','Basic',15,'5 to 8','P1 to P3','','Mr.Rahul','',45,130,'4.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR008','100002','Daisy Pvt.Ltd','Language','French','Basic',10,'5 to 8','P1 to P3','','Ms.Stacy','',45,100,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR009','100002','Daisy Pvt.Ltd','Language','German','Basic',10,'5 to 8','P1 to P3','','Mr.Lee','',45,150,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR010','100002','Daisy Pvt.Ltd','Language','Hindi','Basic',10,'5 to 8','P1 to P3','','Ms.Kalinda','',45,130,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR011','100002','Daisy Pvt.Ltd','Language','Tamil','Basic',10,'5 to 8','P1 to P3','','Mdm.Sherly','',45,130,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR012','100002','Daisy Pvt.Ltd','Language','English','Basic',10,'5 to 8','P1 to P3','','Mr.Peter','',45,130,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR013','100003','ABC Pvt.Ltd','Sports','Soccer','Basic',25,'5 to 8','P1 to P3','','Mr.Ralph','',60,140,'4.00PM','weekly','Tampines');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR014','100003','ABC Pvt.Ltd','Sports','Badminton','Basic',10,'5 to 8','P1 to P3','','Mr.Paul','',45,130,'4.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR015','100003','ABC Pvt.Ltd','Sports','Swimming','Basic',12,'5 to 8','P1 to P3','','Mr.Robert','',45,130,'4.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR016','100003','ABC Pvt.Ltd','Sports','Tennis','Basic',14,'5 to 8','P1 to P3','','Mr.Loius','',45,120,'4.00PM','weekly','Tampines');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR017','100003','ABC Pvt.Ltd','Sports','Chess','Basic',10,'5 to 8','P1 to P3','','Ms.Patricia','',45,130,'4.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR018','100003','ABC Pvt.Ltd','Sports','Table Tennis','Basic',12,'5 to 8','P1 to P3','','Mr.Syed','',45,160,'4.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR019','100005','Sparkle Pvt.Ltd','Academics','Maths','Basic',15,'5 to 8','P1 to P3','','Ms.Grace','',45,130,'4.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR020','100005','Sparkle Pvt.Ltd','Academics','Reading','NA',5,'4 to 6','K1 to K2','','Ms.Murphy','',30,200,'4.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR021','100005','Sparkle Pvt.Ltd','Academics','Science','Basic',15,'5 to 8','P1 to P3','','Ms.Lathika','',45,130,'4.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR022','100010','Bright Pvt.Ltd','Academics','Maths','Basic',15,'5 to 8','P1 to P3','','Mr.Aung','',45,130,'4.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR023','100010','Bright Pvt.Ltd','Academics','Reading','NA',5,'4 to 6','K1 to K2','','Ms.Charlize','',30,180,'4.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR024','100010','Bright Pvt.Ltd','Academics','Science','Basic',10,'5 to 8','P1 to P3','','Mr.Frank','',45,130,'4.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR025','100000','Lotus Pvt.Ltd','Language','Mandarin','Intermediate',10,'9 to 11','P4 to P5','','Mr.Tan','',45,130,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR026','100000','Lotus Pvt.Ltd','Language','French','Intermediate',15,'9 to 11','P4 to P5','','Ms.Chung','',45,130,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR027','100000','Lotus Pvt.Ltd','Language','German','Intermediate',15,'10 to 11','P4 to P5','','Ms.Patel','',45,130,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR028','100000','Lotus Pvt.Ltd','Language','Hindi','Intermediate',15,'11 to 11','P4 to P5','','Ms.Sarita','',45,100,'4.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR029','100001','Lily Pvt.Ltd','Art','Speech and Drama','Intermediate',5,'12 to 11','P4 to P5','','Ms.Ashie','',45,120,'4.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR030','100001','Lily Pvt.Ltd','Art','Visual Arts','Intermediate',15,'13 to 11','P4 to P5','','Mr.Rahul','',45,130,'4.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR031','100002','Daisy Pvt.Ltd','Language','French','Intermediate',10,'14 to 11','P4 to P5','','Ms.Stacy','',45,100,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR032','100002','Daisy Pvt.Ltd','Language','German','Intermediate',10,'15 to 11','P4 to P5','','Mr.Lee','',45,150,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR033','100002','Daisy Pvt.Ltd','Language','Hindi','Intermediate',10,'16 to 11','P4 to P5','','Ms.Kalinda','',45,130,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR034','100002','Daisy Pvt.Ltd','Language','Tamil','Intermediate',10,'17 to 11','P4 to P5','','Mdm.Sherly','',45,130,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR035','100002','Daisy Pvt.Ltd','Language','English','Intermediate',10,'18 to 11','P4 to P5','','Mr.Peter','',45,130,'4.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR036','100003','ABC Pvt.Ltd','Sports','Swimming','Intermediate',12,'19 to 11','P4 to P5','','Mr.Robert','',45,130,'4.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR037','100005','Sparkle Pvt.Ltd','Academics','Maths','intermediate',15,'20 to 11','P4 to P5','','Ms.Grace','',45,130,'4.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR038','100005','Sparkle Pvt.Ltd','Academics','Science','intermediate',15,'21 to 11','P4 to P5','','Ms.Lathika','',45,130,'4.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR039','100010','Bright Pvt.Ltd','Academics','Maths','intermediate',15,'22 to 11','P4 to P5','','Mr.Aung','',45,130,'4.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR040','100010','Bright Pvt.Ltd','Academics','Science','intermediate',10,'23 to 11','P4 to P5','','Mr.Frank','',45,130,'4.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR041','100000','Lotus Pvt.Ltd','Language','Mandarin','Basic',10,'5 to 8','P1 to P3','','Mr.Tan','',45,130,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR042','100000','Lotus Pvt.Ltd','Language','French','Basic',15,'5 to 8','P1 to P3','','Ms.Chung','',45,130,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR043','100000','Lotus Pvt.Ltd','Language','German','Basic',15,'5 to 8','P1 to P3','','Ms.Patel','',45,130,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR044','100000','Lotus Pvt.Ltd','Language','Hindi','Basic',15,'5 to 8','P1 to P3','','Ms.Sarita','',45,100,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR045','100001','Lily Pvt.Ltd','Art','Speech and Drama','Basic',5,'5 to 8','P1 to P3','','Ms.Ashie','',45,120,'5.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR046','100001','Lily Pvt.Ltd','Art','Culinary Arts','Basic',10,'5 to 8','P1 to P3','','Ms.Ancy','',30,130,'5.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR047','100001','Lily Pvt.Ltd','Art','Visual Arts','Basic',15,'5 to 8','P1 to P3','','Mr.Rahul','',45,130,'5.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR048','100002','Daisy Pvt.Ltd','Language','French','Basic',10,'5 to 8','P1 to P3','','Ms.Stacy','',45,100,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR049','100002','Daisy Pvt.Ltd','Language','German','Basic',10,'5 to 8','P1 to P3','','Mr.Lee','',45,150,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR050','100002','Daisy Pvt.Ltd','Language','Hindi','Basic',10,'5 to 8','P1 to P3','','Ms.Kalinda','',45,130,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR051','100002','Daisy Pvt.Ltd','Language','Tamil','Basic',10,'5 to 8','P1 to P3','','Mdm.Sherly','',45,130,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR052','100002','Daisy Pvt.Ltd','Language','English','Basic',10,'5 to 8','P1 to P3','','Mr.Peter','',45,130,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR053','100003','ABC Pvt.Ltd','Sports','Soccer','Basic',25,'5 to 8','P1 to P3','','Mr.Ralph','',60,140,'5.00PM','weekly','Tampines');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR054','100003','ABC Pvt.Ltd','Sports','Badminton','Basic',10,'5 to 8','P1 to P3','','Mr.Paul','',45,130,'5.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR055','100003','ABC Pvt.Ltd','Sports','Swimming','Basic',12,'5 to 8','P1 to P3','','Mr.Robert','',45,130,'5.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR056','100003','ABC Pvt.Ltd','Sports','Tennis','Basic',14,'5 to 8','P1 to P3','','Mr.Loius','',45,120,'5.00PM','weekly','Tampines');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR057','100003','ABC Pvt.Ltd','Sports','Chess','Basic',10,'5 to 8','P1 to P3','','Ms.Patricia','',45,130,'5.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR058','100003','ABC Pvt.Ltd','Sports','Table Tennis','Basic',12,'5 to 8','P1 to P3','','Mr.Syed','',45,160,'5.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR059','100005','Sparkle Pvt.Ltd','Academics','Maths','Basic',15,'5 to 8','P1 to P3','','Ms.Grace','',45,130,'5.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR060','100005','Sparkle Pvt.Ltd','Academics','Reading','NA',5,'4 to 6','K1 to K2','','Ms.Murphy','',30,200,'5.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR061','100005','Sparkle Pvt.Ltd','Academics','Science','Basic',15,'5 to 8','P1 to P3','','Ms.Lathika','',45,130,'5.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR062','100010','Bright Pvt.Ltd','Academics','Maths','Basic',15,'5 to 8','P1 to P3','','Mr.Aung','',45,130,'5.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR063','100010','Bright Pvt.Ltd','Academics','Reading','NA',5,'4 to 6','K1 to K2','','Ms.Charlize','',30,180,'5.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR064','100010','Bright Pvt.Ltd','Academics','Science','Basic',10,'5 to 8','P1 to P3','','Mr.Frank','',45,130,'5.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR065','100000','Lotus Pvt.Ltd','Language','Mandarin','Intermediate',10,'9 to 11','P4 to P5','','Mr.Tan','',45,130,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR066','100000','Lotus Pvt.Ltd','Language','French','Intermediate',15,'9 to 11','P4 to P5','','Ms.Chung','',45,130,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR067','100000','Lotus Pvt.Ltd','Language','German','Intermediate',15,'10 to 11','P4 to P5','','Ms.Patel','',45,130,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR068','100000','Lotus Pvt.Ltd','Language','Hindi','Intermediate',15,'11 to 11','P4 to P5','','Ms.Sarita','',45,100,'5.00PM','weekly','Serangoon');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR069','100001','Lily Pvt.Ltd','Art','Speech and Drama','Intermediate',5,'12 to 11','P4 to P5','','Ms.Ashie','',45,120,'5.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR070','100001','Lily Pvt.Ltd','Art','Visual Arts','Intermediate',15,'13 to 11','P4 to P5','','Mr.Rahul','',45,130,'5.00PM','weekly','Punggol');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR071','100002','Daisy Pvt.Ltd','Language','French','Intermediate',10,'14 to 11','P4 to P5','','Ms.Stacy','',45,100,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR072','100002','Daisy Pvt.Ltd','Language','German','Intermediate',10,'15 to 11','P4 to P5','','Mr.Lee','',45,150,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR073','100002','Daisy Pvt.Ltd','Language','Hindi','Intermediate',10,'16 to 11','P4 to P5','','Ms.Kalinda','',45,130,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR074','100002','Daisy Pvt.Ltd','Language','Tamil','Intermediate',10,'17 to 11','P4 to P5','','Mdm.Sherly','',45,130,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR075','100002','Daisy Pvt.Ltd','Language','English','Intermediate',10,'18 to 11','P4 to P5','','Mr.Peter','',45,130,'5.00PM','weekly','Harbour Front');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR076','100003','ABC Pvt.Ltd','Sports','Swimming','Intermediate',12,'19 to 11','P4 to P5','','Mr.Robert','',45,130,'5.00PM','weekly','Sengkang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR077','100005','Sparkle Pvt.Ltd','Academics','Maths','intermediate',15,'20 to 11','P4 to P5','','Ms.Grace','',45,130,'5.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR078','100005','Sparkle Pvt.Ltd','Academics','Science','intermediate',15,'21 to 11','P4 to P5','','Ms.Lathika','',45,130,'5.00PM','weekly','Jurong East');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR079','100010','Bright Pvt.Ltd','Academics','Maths','intermediate',15,'22 to 11','P4 to P5','','Mr.Aung','',45,130,'5.00PM','weekly','Hougang');
INSERT INTO marvel_courses_db.courses(`id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location`) VALUES ('CR080','100010','Bright Pvt.Ltd','Academics','Science','intermediate',10,'23 to 11','P4 to P5','','Mr.Frank','',45,130,'5.00PM','weekly','Hougang');

