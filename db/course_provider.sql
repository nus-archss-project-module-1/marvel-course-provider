drop database marvel_courses_db;
create database marvel_courses_db;
use marvel_courses_db;

drop
user marvel_courses_rw;
CREATE
USER marvel_courses_rw IDENTIFIED BY 'marvel';

GRANT ALL PRIVILEGES ON marvel_courses_db.* TO
'marvel_courses_rw';

drop
user marvel_user_rw;
CREATE
USER marvel_user_rw IDENTIFIED BY 'marvel';


drop table marvel_courses_db.course_provider;
CREATE TABLE marvel_courses_db.course_provider
(
    id     			varchar(25)  NOT NULL ,
    name   			varchar(128) ,
    license_number   varchar(45)  ,
    license_document    varchar(128) ,
    contact_number    varchar(10)  ,
    email          varchar(50)  ,
    location       varchar(20) ,
    country        varchar(10)  ,
    address        varchar(100)  ,
    postal_code    int,
    user_password  varchar(10),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    -- ,UNIQUE KEY email_UNIQUE (email)
);

desc marvel_courses_db.course_provider;

drop table marvel_courses_db.login_details;
CREATE TABLE marvel_courses_db.login_details
(
    user_name  varchar(80) NOT NULL,
    user_password  varchar(10),
    id varchar(25) not null,
    name  varchar(80) not null,
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (user_name)
);

alter table marvel_courses_db.courses modify column id varchar(25);

drop table marvel_courses_db.courses;
CREATE TABLE marvel_courses_db.courses
(
    id     			varchar(25)  NOT NULL,
    course_provider_id varchar(25)  NOT NULL, -- FK to Course_Provider
    name   			varchar(128) ,
    category        varchar(128)  ,
    sub_category    varchar(128) ,
    level varchar(20),
    class_size      int  ,
    age_group       varchar(50)  ,
    grades          varchar(10) ,
    pre_requisite   varchar(128),
    teacher         varchar(128)  ,
    modules         varchar(128)  ,
    duration        int  ,
    fee             int  ,
    slots varchar(10),
    frequency varchar(10),
    location varchar(20),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    -- ,UNIQUE KEY email_UNIQUE (email)
);
select * from marvel_courses_db.course_provider ;
select * from marvel_courses_db.login_details ; 
select * from marvel_courses_db.courses ;
select * from marvel_courses_db.course_enrolment ;
select * from marvel_courses_db.course_category;
select * from marvel_courses_db.course_sub_category;
-- truncate table marvel_courses_db.course_enrolment;


select * from marvel_courses_db.course_search_history;
select distinct student_id, category, sub_category, age_group, location from marvel_courses_db.course_search_history;
delete from marvel_courses_db.course_search_history where category is null;

drop table marvel_courses_db.course_search_history;
create table marvel_courses_db.course_search_history(
	id varchar(25)  NOT NULL,
    student_id varchar(25)  NOT NULL,
    category varchar(50),
    sub_category varchar(50),
    age_group varchar(10),
    location varchar(20),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP
);

select * from marvel_courses_db.course_provider where id='100000';
select distinct sub_category from marvel_courses_db.courses;
select * from marvel_courses_db.courses where sub_category in ('Guitar', 'Ukelele');
select * from marvel_courses_db.courses where location = 'Tampines';
select * from marvel_courses_db.courses ;

select * from marvel_courses_db.courses  where category like "%null%";
select c.*, cp.name as course_provider_name from  marvel_courses_db.courses c join marvel_courses_db.course_provider cp
	on c.course_provider_id = cp.id where c.course_provider_id='100000';


-- student enrollement
select ce.id, c.name, c.id, ce.student_id, ce.student_name, ce.gender, ce.parent_name,
ce.contact_number, ce.email, c.teacher from  marvel_courses_db.course_enrolment ce join marvel_courses_db.courses c on ce.course_id = c.id
    join marvel_courses_db.course_provider cp on c.course_provider_id = cp.id
where c.course_provider_id in ('100000');


SET SQL_SAFE_UPDATES = 0;
update  marvel_courses_db.login_details set id='100000', name='Lotus Pvt.Ltd', user_name='lotus', user_password='lotus';
update marvel_courses_db.course_provider set email='lotus', user_password='lotus' where id='100000';
update marvel_courses_db.course_provider set location='Tampines' WHERE id = '1618823401648';
update marvel_courses_db.courses set location='Tampines' WHERE sub_category = 'Tennis';
update marvel_courses_db.courses set sub_category='Reading' where sub_category='Early Reader Programme';
SET SQL_SAFE_UPDATES = 1;

alter table marvel_courses_db.course_provider rename column district to location;

drop table marvel_courses_db.course_enrolment;
create table marvel_courses_db.course_enrolment(
	id varchar(25)  NOT NULL,
    course_id varchar(25)  NOT NULL,
    student_id varchar(25)  NOT NULL,
    student_name   varchar(80) ,
    gender         varchar(6) ,
	parent_name    varchar(128) ,
    contact_number varchar(10) ,
    email          varchar(80)  ,
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    primary key (id),
    UNIQUE KEY unique_idx (student_id, course_id)    
);

drop table marvel_courses_db.course_category;
create table marvel_courses_db.course_category(
	category_id int not null auto_increment,
    category_name varchar(20),
   student_id varchar(25)  NOT NULL,
    primary key (category_id),
    created_time timestamp DEFAULT CURRENT_TIMESTAMP
);

drop table marvel_courses_db.course_sub_category;
create table marvel_courses_db.course_sub_category(
	subcategory_id int not null auto_increment,
    category_id	int not null,
    subcategory_name varchar(50),
    primary key (subcategory_id),
    UNIQUE KEY unique_idx (subcategory_id, category_id)
);

drop table marvel_courses_db.course_sub_category;
create table marvel_courses_db.course_sub_category(
	subcategory_id int not null auto_increment,
    category_id	int not null,
    subcategory_name varchar(50),
    primary key (subcategory_id),
    UNIQUE KEY unique_idx (subcategory_id, category_id)
);



-- Used queries
select * from marvel_courses_db.courses c where (
            'Sports'='' 
            or c.category='Sports'
        ) 
        and (
            'Badminton'='' 
            or c.sub_category= 'Badminton'
        ) 
        and (
            'Sengkang'='' 
            or c.location= 'Sengkang'
        ) 
        and (
            '5 to 11'='' 
            or c.age_group='5 to 8'
        );
        
        

        
-- Course Provider 
truncate table marvel_courses_db.course_provider;
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100000','Lotus Pvt.Ltd','7879728','','9111111','a','Serangoon','Singapore','123 yuw Road',822101,'a');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100001','Lily Pvt.Ltd','1279737','','9222222','lily@email.com','Punggol','Singapore','124 yuw Road',819828,'Li2@');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100002','Daisy Pvt.Ltd','6928264','','9333333','Daisy@email.com','Harbor Front','Singapore','125 yuw Road',811110,'Da3#');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100003','ABC Pvt.Ltd','9122739','','9444444','ABC@email.com','Sengkang','Singapore','126 yuw Road',818137,'AB4$');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100004','XYZ Pvt.Ltd','9817371','','9555555','XYZ@email.com','Punggol','Singapore','127 yuw Road',812390,'XY5%');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100005','Sparkle Pvt.Ltd','7678161','','9666666','Sparkle@email.com','Jurong East','Singapore','128 yuw Road',876543,'Sp6^');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100006','Unicorn Pvt.Ltd','8789191','','9777777','Unicorn@email.com','Serangoon','Singapore','129 yuw Road',812083,'Un7&');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100007','Austria Pvt.Ltd','8798319','','9888888','Austria@email.com','Harbor Front','Singapore','130 yuw Road',864103,'Au8*');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100008','Sunshine Pvt.Ltd','3451152','','9000000','Sunshine@email.com','Jurong East','Singapore','131 yuw Road',834502,'Su9(');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100009','Rainbow Pvt.Ltd','6273521','','9999999','rainbow@email.com','Toa Payoh','Singapore','132 yuw Road',800374,'Ra0)');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100010','Bright Pvt.Ltd','8651825','','9012345','Bright@email.com','Hougang','Singapore','133 yuw Road',877740,'Br7:');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('100011','Academia Pvt.Ltd','4651421','','9876543','Academia@email.com','Toa Payoh','Singapore','134 yuw Road',811110,'Ac4>');
INSERT INTO marvel_courses_db.course_provider(`id`,`name`,`license_number`,`license_document`,`contact_number`,`email`,`location`,`country`,`address`,`postal_code`,`user_password`) VALUES ('1618823401648','sadfdsfds','','','','a','Tampines','','',0,'a');


-- Courses
select `id`,`course_provider_id`,`name`,`category`,`sub_category`,`level`,`class_size`,`age_group`,`grades`,`pre_requisite`,`teacher`,`modules`,`duration`,`fee`,`slots`,`frequency`,`location` from marvel_courses_db.courses;